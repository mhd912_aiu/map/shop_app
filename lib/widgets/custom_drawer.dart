import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shop_app/pages/home_page.dart';
import 'package:shop_app/pages/settings_page.dart';

class CustomDrawer extends Drawer {
  CustomDrawer({
    super.key,
    required Type runtimeType,
    required BuildContext context,
  }) : super(
          child: ListView(
            scrollDirection: Axis.vertical,
            children: [
              Container(
                height: 100,
                color: Colors.blue,
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.only(left: 15),
                child: const Text(
                  "Shoppy",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              _listTile(
                titleText: "Home",
                leadingIcon: Icons.home,
                widget: HomePage(),
                runtimeType: runtimeType,
                context: context,
              ),
              _listTile(
                  titleText: "Settings",
                  leadingIcon: Icons.settings,
                  widget: const SettingsPage(),
                  runtimeType: runtimeType,
                  context: context)
            ],
          ),
        );

  static ListTile _listTile({
    required String titleText,
    required IconData leadingIcon,
    required Widget widget,
    required Type runtimeType,
    required BuildContext context,
  }) {
    return ListTile(
      leading: Icon(leadingIcon),
      title: Text(
        titleText,
        style: const TextStyle(
          fontSize: 16,
        ),
      ),
      onTap: () {
        if (runtimeType != widget.runtimeType) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => widget,
            ),
          );
        } else {
          Navigator.of(context).pop();
        }
      },
    );
  }
}
