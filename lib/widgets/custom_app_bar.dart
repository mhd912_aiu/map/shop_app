import 'package:flutter/material.dart';

class CustomAppBar extends AppBar {
  CustomAppBar({
    super.key,
    required String titleText,
    List<Widget>? actionsList,
  }) : super(
          title: Text(
            titleText,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          iconTheme: const IconThemeData(
            color: Colors.white,
          ),
          actions: actionsList,
          elevation: 0,
          scrolledUnderElevation: 0,
          backgroundColor: Colors.blue,
          surfaceTintColor: Colors.transparent,
        );
}
