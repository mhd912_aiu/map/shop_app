class Item {
  String _name;
  String _imageUrl;
  String _description;

  Item({
    required String name,
    required String imageUrl,
    required String description,
  })  : _name = name,
        _imageUrl = imageUrl,
        _description = description;

  String get getName => _name;
  String get getImageUrl => _imageUrl;
  String get getDescription => _description;

  set setName(String name) => _name = name;
  set setImageUrl(String url) => _imageUrl = url;
  set setDescription(String description) => _description = description;
}
