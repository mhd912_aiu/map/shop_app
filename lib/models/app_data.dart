import 'package:flutter/material.dart';
import 'package:shop_app/models/item.dart';
import 'package:shop_app/models/item_category.dart';

class AppData {
  List<ItemCategory> categoriesList = [
    ItemCategory(
      name: "Displays",
      icon: Icons.phone_android,
    ),
    ItemCategory(
      name: "Battery",
      icon: Icons.battery_4_bar,
    ),
    ItemCategory(
      name: "Cables",
      icon: Icons.cable,
    ),
    ItemCategory(
      name: "Headphones",
      icon: Icons.headphones,
    ),
    ItemCategory(
      name: "Mice",
      icon: Icons.mouse,
    ),
    ItemCategory(
      name: "Keyboards",
      icon: Icons.keyboard,
    ),
  ];
  List<Item> itemsList = [
    Item(
      name: "Item1",
      imageUrl: "assets/images/product.jpg",
      description:
          "Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah",
    ),
    Item(
      name: "Item2",
      imageUrl: "assets/images/product.jpg",
      description:
          "Blah Blah Blah Blah Blah Nice Weather Blah Blah Blah Blah not so much",
    ),
    Item(
      name: "Item3",
      imageUrl: "assets/images/product.jpg",
      description: "Blah Blah Blah Blah Blah Blah Blah",
    ),
    Item(
      name: "Item4",
      imageUrl: "assets/images/product.jpg",
      description:
          "Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah",
    ),
    Item(
      name: "Item5",
      imageUrl: "assets/images/product.jpg",
      description:
          "WeooeW Blah Blah WeooeW WeooeW Blah Blah WeooeW Blah Blah Blah",
    ),
    Item(
      name: "Item6",
      imageUrl: "assets/images/product.jpg",
      description:
          "WeooeW Blah Blah Blah Blah Blah WeooeW WeooeW WeooeW WeooeW ",
    ),
  ];
}
