import 'package:flutter/material.dart';

class ItemCategory {
  final String _name;
  final IconData _icon;

  ItemCategory({
    required String name,
    required IconData icon,
  })  : _name = name,
        _icon = icon;
  String get getName => _name;
  IconData get getIcon => _icon;
}
