import 'package:flutter/material.dart';
import 'package:shop_app/models/app_data.dart';
import 'package:shop_app/models/item_category.dart';
import 'package:shop_app/pages/items_page.dart';
import 'package:shop_app/widgets/custom_app_bar.dart';
import 'package:shop_app/widgets/custom_drawer.dart';

late double _deviceWidth, _deviceHeight;

class HomePage extends StatelessWidget {
  final AppData _appData;
  HomePage({super.key}) : _appData = AppData();

  @override
  Widget build(BuildContext context) {
    _deviceHeight = MediaQuery.of(context).size.height;
    _deviceWidth = MediaQuery.of(context).size.width;
    return _buildPage(context);
  }

  Widget _buildPage(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        titleText: "Profile",
      ),
      drawer: CustomDrawer(
        context: context,
        runtimeType: runtimeType,
      ),
      body: SizedBox.expand(
        child: _categoriesGridViewWidget(),
      ),
    );
  }

  Widget _categoriesGridViewWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 25,
        vertical: 10,
      ),
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),
        itemCount: _appData.categoriesList.length,
        itemBuilder: (context, index) {
          return _gridViewItem(context, index);
        },
      ),
    );
  }

  Widget _gridViewItem(BuildContext context, int index) {
    return MaterialButton(
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => ItemsPage(
              categoryName: _appData.categoriesList[index].getName,
              appData: _appData,
            ),
          ),
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      color: Colors.blueGrey.shade50,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            _appData.categoriesList[index].getIcon,
            color: Colors.blue,
            size: 35,
          ),
          const SizedBox(height: 10),
          Text(
            _appData.categoriesList[index].getName,
            style: const TextStyle(fontSize: 14),
          ),
        ],
      ),
    );
  }
}
