import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/controllers/item_details_controller.dart';
import 'package:shop_app/models/item.dart';
import 'package:shop_app/widgets/custom_app_bar.dart';

late double _deviceWidth, _deviceHeight;
late ItemDetailsController _controller;

class ItemDetailsPage extends StatelessWidget {
  final Item _item;
  const ItemDetailsPage({
    super.key,
    required Item item,
  }) : _item = item;

  @override
  Widget build(BuildContext context) {
    _deviceWidth = MediaQuery.of(context).size.width;
    _deviceHeight = MediaQuery.of(context).size.height;
    return ChangeNotifierProvider(
      create: (context) => ItemDetailsController(item: _item),
      child: Builder(
        builder: (context) {
          _controller = context.watch<ItemDetailsController>();
          return _buildPage(context);
        },
      ),
    );
  }

  Widget _buildPage(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        titleText: _item.getName,
        actionsList: [
          IconButton(
            onPressed: () {
              _editInfo(context);
            },
            icon: Icon(
              Icons.edit_square,
              color: Colors.grey.shade300,
            ),
          )
        ],
      ),
      body: SizedBox.expand(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: _deviceHeight * 0.3,
                width: _deviceWidth,
                decoration: const BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                ),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        _item.getImageUrl,
                      ),
                    ),
                    // shape: BoxShape.rectangle,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(height: 25),
                    _pageItem(
                      icon: Icons.note,
                      title: _item.getName,
                      content: "",
                    ),
                    const SizedBox(height: 15),
                    _pageItem(
                      icon: Icons.manage_search_outlined,
                      title: "Description",
                      content: _item.getDescription,
                    ),
                    const SizedBox(height: 50),
                    _buyButton(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container _pageItem({
    required IconData icon,
    required String title,
    required String content,
  }) {
    return Container(
      padding: EdgeInsets.all(
        (content.isNotEmpty) ? 20 : 10,
      ),
      decoration: BoxDecoration(
        color: Colors.blueGrey.shade50,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(width: 15),
              Icon(
                size: 25,
                icon,
              ),
              const SizedBox(width: 20),
              Text(
                title,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(
              (content.isNotEmpty) ? 20 : 0,
            ),
            child: Text(
              content,
              style: const TextStyle(fontSize: 14),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buyButton() {
    return MaterialButton(
      onPressed: () {},
      color: Colors.blue,
      padding: const EdgeInsets.symmetric(
        horizontal: 30,
        vertical: 20,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: const Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.shopping_basket,
            color: Colors.white,
            size: 20,
          ),
          SizedBox(width: 15),
          Text(
            "Buy Now!",
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  _editInfo(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text(
            "Edit",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 40),
              _textFieldWidget(
                labelText: "Name",
                textController: _controller.nameController,
              ),
              const SizedBox(height: 20),
              _textFieldWidget(
                labelText: "Description",
                textController: _controller.descriptionController,
              ),
              const SizedBox(height: 20),
            ],
          ),
          actionsAlignment: MainAxisAlignment.spaceEvenly,
          actions: [
            MaterialButton(
              onPressed: () {
                Navigator.of(context).pop();
                _controller.modifyItem();
              },
              color: Colors.green,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: const Text(
                "Save",
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
              ),
            ),
            MaterialButton(
              onPressed: () {
                _controller.nameController.text = _item.getName;
                _controller.descriptionController.text = _item.getDescription;
                Navigator.of(context).pop();
              },
              color: Colors.blue,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: const Text(
                "Cancel",
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  TextField _textFieldWidget({
    required String labelText,
    required TextEditingController textController,
  }) {
    return TextField(
      autofocus: false,
      controller: textController,
      decoration: InputDecoration(
        label: Text(
          labelText,
          style: const TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 18,
          ),
        ),
        filled: true,
        fillColor: Colors.blue.shade100,
        focusColor: Colors.blue.shade100,
        hoverColor: Colors.blue.shade100,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      style: const TextStyle(
        fontSize: 14,
      ),
    );
  }
}
