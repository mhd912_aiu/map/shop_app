import 'package:flutter/material.dart';
import 'package:shop_app/widgets/custom_app_bar.dart';
import 'package:shop_app/widgets/custom_drawer.dart';

late double _deviceWidth, _deviceHeight;

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    _deviceHeight = MediaQuery.of(context).size.height;
    _deviceWidth = MediaQuery.of(context).size.width;
    return _buildPage(context);
  }

  Widget _buildPage(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        titleText: "Settings",
      ),
      // drawer: CustomDrawer(
      //   runtimeType: runtimeType,
      //   context: context,
      // ),
      body: SafeArea(
        child: Container(
          height: _deviceHeight,
          width: _deviceWidth,
          margin: EdgeInsets.symmetric(
            horizontal: _deviceWidth * 0.05,
            vertical: _deviceHeight * 0.1,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Theme mode:",
                style: TextStyle(
                  fontSize: 25,
                ),
              ),
              _themeModeOptionsWidget(
                radioValue: "system",
                titleText: "النظام",
              ),
              _themeModeOptionsWidget(
                radioValue: "light",
                titleText: "فاتح",
              ),
              _themeModeOptionsWidget(
                radioValue: "dark",
                titleText: "داكن",
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _themeModeOptionsWidget({
    required String radioValue,
    required String titleText,
  }) {
    return RadioListTile(
      value: radioValue,
      groupValue: 'system',
      title: Text(titleText),
      onChanged: (newValue) {},
    );
  }
}
