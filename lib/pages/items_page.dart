import 'package:flutter/material.dart';
import 'package:shop_app/models/app_data.dart';
import 'package:shop_app/pages/item_details_page.dart';
import 'package:shop_app/widgets/custom_app_bar.dart';

late double _deviceWidth, _deviceHeight;
late String _categoryName;
late AppData _appData;

class ItemsPage extends StatefulWidget {
  ItemsPage({
    super.key,
    required String categoryName,
    required AppData appData,
  }) {
    _categoryName = categoryName;
    _appData = appData;
  }
  @override
  State<StatefulWidget> createState() {
    return _ItemsPageState();
  }
}

class _ItemsPageState extends State<ItemsPage> {
  _ItemsPageState();
  @override
  Widget build(BuildContext context) {
    _deviceHeight = MediaQuery.of(context).size.height;
    _deviceWidth = MediaQuery.of(context).size.width;
    return _buildPage(context);
  }

  Widget _buildPage(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        titleText: _categoryName,
      ),
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 25,
            vertical: 10,
          ),
          child: ListView.builder(
            itemCount: _appData.itemsList.length,
            itemBuilder: (context, index) {
              return _listViewItem(context, index);
            },
          ),
        ),
      ),
    );
  }

  Widget _listViewItem(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: MaterialButton(
        onPressed: () async {
          await Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => ItemDetailsPage(
                item: _appData.itemsList[index],
              ),
            ),
          );
          setState(() {});
        },
        height: 150,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        clipBehavior: Clip.antiAlias,
        padding: EdgeInsets.zero,
        color: Colors.blueGrey.shade50,
        child: Stack(
          alignment: Alignment.bottomLeft,
          children: [
            Container(
              height: _deviceHeight * 0.3,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage(
                    _appData.itemsList[index].getImageUrl,
                  ),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 25,
                vertical: 5,
              ),
              decoration: const BoxDecoration(
                color: Color.fromRGBO(2, 2, 2, 0.5),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                ),
              ),
              child: Text(
                _appData.itemsList[index].getName,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
