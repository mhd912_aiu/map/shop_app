import 'package:flutter/material.dart';
import 'package:shop_app/models/item.dart';

class ItemDetailsController extends ChangeNotifier {
  final TextEditingController nameController;
  final TextEditingController descriptionController;
  final Item _item;
  ItemDetailsController({required Item item})
      : nameController = TextEditingController(
          text: item.getName,
        ),
        descriptionController = TextEditingController(
          text: item.getDescription,
        ),
        _item = item;

  modifyItem() {
    _item.setName = nameController.text;
    _item.setDescription = descriptionController.text;
    notifyListeners();
  }
}
